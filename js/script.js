document.getElementsByClassName('burger-menu')[0].onclick = function() {
    document.getElementsByClassName('burger-control')[0].classList.add('burger-open');
}

document.getElementsByClassName('burger-close')[0].onclick = function() {
    document.getElementsByClassName('burger-control')[0].classList.remove('burger-open');
}

document.getElementsByClassName('header-logo')[0].onclick = function() {
    document.getElementsByClassName('wrapper')[0].classList.add('hidden-wrapper');
}

document.getElementsByClassName('section')[0].onclick = function() {
    document.getElementsByClassName('wrapper')[0].classList.remove('hidden-wrapper');
}

document.addEventListener("mousemove" , parallax);
function parallax(e){
    this.querySelectorAll('.layer').forEach(layer => {
        const speed = layer.getAttribute('data-speed')

        const x = (window.innerWidth - e.pageX*speed)/80
        const y = (window.innerHeight - e.pageY*speed)/80

        layer.style.transform = `translateX(${x}px) translateY(${y}px)`
    });
}


document.addEventListener('DOMContentLoaded', () => { 

    const onScrollHeader = () => { 
  
      const header = document.querySelector('.header') 
  
      let prevScroll = window.pageYOffset 
      let currentScroll 
  
      window.addEventListener('scroll', () => { 
  
        currentScroll = window.pageYOffset 
  
        const headerHidden = () => header.classList.contains('header_hidden')
  
        if (currentScroll > prevScroll && !headerHidden()) { 
          header.classList.add('header_hidden') 
        }
        if (currentScroll < prevScroll && headerHidden()) { 
          header.classList.remove('header_hidden') 
        }
  
        prevScroll = currentScroll 
  
      })
  
    }
    onScrollHeader() 
  });

// let scrollpos = window.scrollY

// const header = document.querySelector("header")

// const scrollChange = 1

// //Функция, которая будет добавлять класс
// const add_class_on_scroll = () => header.classList.add("bg-red")

// //Отслеживаем событие "скролл"
// window.addEventListener('scroll', function() { 
//   scrollpos = window.scrollY;

//   //Если прокрутили больше, чем мы указали в переменной scrollChange, то выполняется функция добавления класса
//   if (scrollpos >= scrollChange) { add_class_on_scroll() }
// })


function onEntry(entry) {
  entry.forEach(change => {
    if (change.isIntersecting) {
     change.target.classList.add('scroll-effect');
    }
  });
}

let options = {
threshold: [0.5] };
let observer = new IntersectionObserver(onEntry, options);
let elements = document.querySelectorAll('.container');
let elementsadd = document.querySelectorAll('.section');

for (let elm of elementsadd) {
observer.observe(elm);
}
