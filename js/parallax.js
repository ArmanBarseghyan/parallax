let parallaxItem1 = document.getElementsByClassName('parallax-item-1')[0];
let parallaxItem2 = document.getElementsByClassName('parallax-item-2')[0];
let parallaxItem3 = document.getElementsByClassName('parallax-item-3')[0];
let parallaxItem4 = document.getElementsByClassName('parallax-item-4')[0];
let parallaxItem5 = document.getElementsByClassName('parallax-item-5')[0];

let btnUnValid = document.getElementsByClassName('btn-unvalid')[0];

window.addEventListener('scroll', function(){
    btnUnValid.style.transform = 'translateY(120%)';
    btnUnValid.style.zIndex = 20;
    btnUnValid.style.opacity = "1";
    // btn_parallax.style.opacity = '1';
    let value = window.scrollY;
    parallaxItem4.style.top = value * 0.3 + 'px';
    parallaxItem3.style.top = value * 0.5 + 'px';
    parallaxItem2.style.top = value * 1 + 'px';
    parallaxItem1.style.top = value * 0.6 + 'px';
    // airhigh.style.top = value * -0.2 + 'px';
    // airhigh.style.left = value * 0.5 + 'px';
    // airlow.style.transform = `scale(${value * 0.009})`;
    // airhigh.style.transform = `scale(${value * 0.009})`;
    // rearBg.style.filterBlur = value * 0.5 + 'blur(px)';
    // rearBg.style.filterBlur = 'blur(' + (value * 0.5) + 'px)';
    // mountainsOne.style.transform = `scale(${value * 0.01})`;
    // mountainsOne.style.width = value * 1 + '%';
    // mountainsOne.style.height = value * 1 + '%';
})



document.addEventListener("mousemove" , parallax);
function parallax(e){
    this.querySelectorAll('.parallax-movie').forEach(layer => {
        const speed = layer.getAttribute('data-speed')

        const x = (window.innerWidth - e.pageX*speed)/80
        const y = (window.innerHeight - e.pageY*speed)/80

        layer.style.transform = `translateX(${x}px) translateY(${y}px)`
    });
}
